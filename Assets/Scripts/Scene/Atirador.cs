﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atirador : MonoBehaviour {

    public GameObject tiro;

    public bool atirar = true;
    public float relogio = 0;
    public float time_spawn = 30;
    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (atirar)
            DarBala();

        
        relogio += Time.deltaTime * 10;
        if (relogio >= time_spawn)
        {
           
            relogio = 0;

            if (atirar)
                atirar = false;
            else
                atirar = true;

            if (time_spawn == 30)
                time_spawn = 20;
            else
                time_spawn = 20;
        }

    }

    void DarBala()
    {
        GameObject bala = Instantiate(tiro, transform.position, transform.rotation);
        bala.transform.Rotate(new Vector3(0, 0, transform.rotation.z + 90));
        Destroy(bala, 2);
    }
}
