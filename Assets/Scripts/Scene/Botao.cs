﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botao : MonoBehaviour {

    public bool ligado = false;
    private float relogio = 0;
    private Color original;
	// Use this for initialization
	void Start () {
        original = GetComponent<Renderer>().material.color;
	}
	
	// Update is called once per frame
	void Update () {
		
        if (ligado)
        {
            GetComponent<Renderer>().material.color = Color.green;
            relogio += Time.deltaTime;
        }


        if (relogio >= 4)
        {
            ligado = false;
            GetComponent<Renderer>().material.color = original;
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "VentoP1" || other.tag == "VentoP2")
        {
            ligado = true;
            relogio = 0;
        }
    }
}
