﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ventilador : MonoBehaviour {

    public bool liga = false;
    float relogio;
    public GameObject bala;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (liga)
        {
            relogio += Time.deltaTime * 10;
            if (relogio > 15)
            {
                Atirar();
                liga = false;
            }
        }
	}

    void Atirar()
    {
        var tiro = Instantiate(bala, transform.position, transform.rotation);
        tiro.transform.Rotate(new Vector3(transform.rotation.z + 90, 0, 0));
        Destroy(tiro, 15);
    }

    public void Liga()
    {
        liga = true;
    }
}
