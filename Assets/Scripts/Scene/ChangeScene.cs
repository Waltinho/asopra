﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public bool gameover = false;
    // Use this for initialization
    void Start () {
        StartCoroutine(Tempo());
	}
	
	// Update is called once per frame
	void Update () {
        if (gameover)
            SceneManager.LoadScene("Prototipo");
    }

    IEnumerator Tempo()
    {
        yield return new WaitForSeconds(2.0f);
        gameover = true;
    }
}
