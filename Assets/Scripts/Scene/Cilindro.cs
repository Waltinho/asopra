﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cilindro : MonoBehaviour {

    float force;
    public GameObject botao1, botao2;
    bool hora = false, anti = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(new Vector3(0, force, 0));

        if (force > 0 && hora)
            force -= Time.deltaTime * 2;
        
        if (force < 0 && anti)
            force += Time.deltaTime * 2;

        if (botao1.GetComponent<Botao>().ligado && botao2.GetComponent<Botao>().ligado)
        {
            force = 5;
        }
	}

    public void GiraHorario()
    {
        force += Time.deltaTime * 4;
        hora = true;
        anti = false;
    }

    public void GiraAntiHorario()
    {
        force -= Time.deltaTime * 4;
        anti = true;
        hora = false;
    }
}
