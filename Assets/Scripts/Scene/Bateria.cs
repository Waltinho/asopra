﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bateria : MonoBehaviour {

    public float impulso = 8.0f;
    public float impulso2 = 10.0f;
    public float speed;
    public GameObject ventila;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Bateria")
            ventila.transform.SendMessage("Liga");
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "VentoP1" || other.tag == "VentoP2")
        {
            if (GetComponent<Rigidbody>().velocity.magnitude < 15)
                GetComponent<Rigidbody>().AddForce(other.transform.forward.normalized * impulso, ForceMode.VelocityChange);

        }

        if (other.tag == "Vento2P2" || other.tag == "Vento2P1")
        {
            transform.Translate(((other.transform.forward.normalized * speed) * impulso2) * Time.deltaTime);
        }

    }
}
