﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sobe : MonoBehaviour {

    public float relogio = 0;
    public float time_spawn = 120;
    public bool sobe = false;
    public bool desce = false;
    public float speed;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        relogio += Time.deltaTime * 10;
        if (relogio > time_spawn)
        {
            relogio = 0;
            if (transform.position.y <= 9.8f)
                sobe = true;
            else if (transform.position.y >= 20)
                desce = true;
        }



        if (sobe)
            transform.Translate(0, Time.deltaTime * speed, 0);

        if (transform.position.y >= 20)
            sobe = false;

        if (desce)
            transform.Translate(0, Time.deltaTime * -speed, 0);

        if (transform.position.y <= 9.8f)
            desce = false;

    }
}
