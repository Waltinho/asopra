﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour {
    public bool sobe = true;
    public float speed;
	// Use this for initialization
	void Start () {
		//-51 e 15
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.z <= 15 && sobe)
            transform.Translate(0,0, Time.deltaTime * speed);

        if (transform.position.z >= 15)
            sobe = false;

        if (!sobe && transform.position.z >= -51)
            transform.Translate(0, 0, Time.deltaTime * -speed);

        if (transform.position.z <= -51)
            sobe = true;
    }
}
