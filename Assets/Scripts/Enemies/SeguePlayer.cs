﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SeguePlayer : MonoBehaviour {

    //NavMeshAgent agent;
    private GameObject player;
    private GameObject player2;

    public float impulso = 30.0f;
    public float impulso2 = 30.0f;
    public float speed;
    public float gravity = 20.0f;
    //34.3 55.4
    private bool persegue = false;
    // Use this for initialization
    void Start () {
        //agent = GetComponent<NavMeshAgent>();
        player = GameObject.Find("MovPlayer").transform.GetChild(0).gameObject;
        player2 = GameObject.Find("MovPlayer2").transform.GetChild(0).gameObject;
        GetComponent<Rigidbody>().useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = player.transform.position - transform.position;
        Vector3 dir2 = player2.transform.position - transform.position;

        if (persegue)
        {
            if (dir.magnitude < dir2.magnitude)
                transform.position += dir.normalized * Time.deltaTime * speed;
            else
                transform.position += dir2.normalized * Time.deltaTime * speed;
        }

        if (transform.position.y <= -35)
        {
            Destroy(this.gameObject);
        }

        GetComponent<Rigidbody>().AddForce(Vector3.down * gravity, ForceMode.Acceleration);
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "VentoP1" || other.tag == "VentoP2")
        {
            if (GetComponent<Rigidbody>().velocity.magnitude < 15)
                GetComponent<Rigidbody>().AddForce(other.transform.forward.normalized * impulso, ForceMode.VelocityChange);

        }

        if (other.tag == "Vento2P2" || other.tag == "Vento2P1")
        {
            transform.Translate(((other.transform.forward.normalized * speed) * impulso2) * Time.deltaTime);
        }

        if (other.tag == "Super")
        {
            GetComponent<Rigidbody>().AddForce(other.transform.up.normalized * 100, ForceMode.VelocityChange);
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Chao")
        {
            persegue = true;
        }
    }
}
