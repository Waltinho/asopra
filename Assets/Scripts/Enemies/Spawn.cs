﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public GameObject bicho;
    public float relogio = 0;
    public float time_spawn = 120;
    // Use this for initialization
    void Start () {
    }
	
	// Update is called once per frame
	void Update () {
        relogio += Time.deltaTime * 10;
        if (relogio > time_spawn)
        {
            Instantiate(bicho, transform.position, Quaternion.identity);
            relogio = 0;
            time_spawn += Random.Range(-30, 30);
            if (time_spawn < 20)
                time_spawn = 20;
            if (time_spawn > 120)
                time_spawn = 120;
        }
    }
    
}
