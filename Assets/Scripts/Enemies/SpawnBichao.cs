﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBichao : MonoBehaviour {

    public GameObject bicho;
    float relogio = 0;
    float time_spawn = 600;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        relogio += Time.deltaTime * 10;

        if (relogio > time_spawn)
        {
            Instantiate(bicho, transform.position, Quaternion.identity);
            relogio = 0;
        }
    }
}
