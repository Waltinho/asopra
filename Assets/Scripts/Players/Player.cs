﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public GameObject target;
    public GameObject target2;
    public GameObject target3;

    public GameObject fire1;
    public GameObject fire2;

    public GameObject barrinha;

    private Vector3 inicial;

    public float duracao_vento1 = 4.0f;
    public float duracao_vento2 = 2.0f;
    public float impulso = 30.0f;
    public float impulso2 = 30.0f;
    public float speed = 8.0f;
    public float gravity = 5.0f;

    private bool rodaRelogio = false;
    private float codeRelogio = 0;

    public float municao = 180.0f;
    private bool recarrega = false;
    private bool cooldown = true;

    private Vector3 mi;
    // Use this for initialization
    void Start()
    {
        inicial = transform.position;
        inicial.y = 35;
        GetComponent<Rigidbody>().useGravity = false;
        mi = barrinha.transform.localScale;
    }

    // Update is called once per frame
    void Update () {
        //MOVIMENTAÇAO BONITA SEM GIRAR
        #region

        var x = Input.GetAxis("Horizontal");
        var z = Input.GetAxis("Vertical");

        //var angle = Mathf.Atan2(z, x);
        //transform.rotation = Quaternion.Slerp(transform.rotation, new Quaternion(0, angle, 0, 0), 50 * Time.deltaTime);
        //transform.Rotate(0, angle, 0);

        Vector3 aponta = new Vector3(x, 0, z);

        if (aponta.magnitude != 0)
            transform.forward = aponta;
        //transform.Translate(-transform.forward.x * 15 * Time.deltaTime, 0, -transform.forward.z * 15 * Time.deltaTime);
 #endregion

        //MOVIMENTAÇAO TANQUE
        #region
        //var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        //var z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        //transform.Rotate(0, x, 0);
        //transform.Translate(0, 0, z);
        #endregion

        //MUNIÇAO TIRO 1
        #region

        if (rodaRelogio)
            codeRelogio += Time.deltaTime;
        
        if (codeRelogio >= 2.5f)
        {
            codeRelogio = 0;
            rodaRelogio = false;
            recarrega = true;
        }

        if (municao <= 0)
        {
            rodaRelogio = true;
        }

        if (recarrega)
        {
            municao += 2;
        }

        if (municao >= 300.0f)
        {
            recarrega = false;
        }
        #endregion

        //Barrinha municao
        barrinha.transform.localScale = new Vector3 (mi.x, mi.y * municao/300, mi.z);
              
        //Debug.Log(municao / 300);

        //Gravidade
        GetComponent<Rigidbody>().AddForce(Vector3.down * gravity, ForceMode.Acceleration);

        if (Input.GetButton("BButton") && municao >= 5 ) {
            Fire2();
            codeRelogio = 0;
            recarrega = false;
            rodaRelogio = false;
        }
        else if (Input.GetButton("AButton") && municao >= 0 ) {
            Fire1();
            codeRelogio = 0;
            recarrega = false;
            rodaRelogio = false;
        }

        if (Input.GetButtonUp("AButton"))
        {
            speed = 8.0f;
            rodaRelogio = true;
        }
        if (Input.GetButtonUp("BButton"))
        {
            rodaRelogio = true;
        }

        if (transform.position.y <= -35)
        {
            transform.position = inicial;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
    }

    void Fire2()
    {
        var tiro = Instantiate(fire2, target.transform.position, target.transform.rotation);
        Destroy(tiro, duracao_vento2);
        tiro.transform.Rotate(new Vector3(0, 0, target.transform.rotation.z + 90));

        var tiro2 = Instantiate(fire2, target2.transform.position, target2.transform.rotation);
        Destroy(tiro2, duracao_vento2);
        tiro2.transform.Rotate(new Vector3(0, 0, target.transform.rotation.z + 90));

        var tiro3 = Instantiate(fire2, target3.transform.position, target3.transform.rotation);
        Destroy(tiro3, duracao_vento2);
        tiro3.transform.Rotate(new Vector3(0, 0, target.transform.rotation.z + 90));

        municao -= 5;
    }

    void Fire1()
    {
        
        GameObject tiro = Instantiate(fire1, target.transform.position, target.transform.rotation);
        tiro.transform.Rotate(new Vector3(0, 0, target.transform.rotation.z + 90));
        Destroy(tiro, duracao_vento1);
        speed = 2.0f;
        municao -= 1;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.transform.tag == "Plataforma")
        {
            if (collision.gameObject.GetComponent<Plataforma>().sobe)
                transform.parent.transform.Translate(0, 0, collision.gameObject.GetComponent<Plataforma>().speed * Time.deltaTime);
            else
                transform.parent.transform.Translate(0, 0, -collision.gameObject.GetComponent<Plataforma>().speed * Time.deltaTime);
        }

        if(collision.transform.tag == "Tramp")
        {
            GetComponent<Rigidbody>().AddForce(transform.up * 4000);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "VentoP2")
        {
            if ( GetComponent<Rigidbody>().velocity.magnitude < 15 )
                GetComponent<Rigidbody>().AddForce(other.transform.forward.normalized * impulso, ForceMode.VelocityChange);
  
        }
        if(other.tag == "Vento2P2")
        {
            if (GetComponent<Rigidbody>().velocity.magnitude < 15)
                GetComponent<Rigidbody>().AddForce(other.transform.forward.normalized * impulso2, ForceMode.VelocityChange);
        }

        if (other.tag == "Tiro")
        {
            if (GetComponent<Rigidbody>().velocity.magnitude < 15)
                GetComponent<Rigidbody>().AddForce(other.transform.forward.normalized * impulso, ForceMode.VelocityChange);

        }

        if (other.tag == "Super")
        {
            GetComponent<Rigidbody>().AddForce(other.transform.up.normalized * 100, ForceMode.VelocityChange);
        }
    }

}
