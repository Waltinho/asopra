﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vento1 : MonoBehaviour {

    public float force;
    Vector3 direcao;
    public bool colidiu = false;
	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        //GetComponent<Rigidbody>().velocity = transform.forward * force;

        transform.position += transform.forward * Time.deltaTime * force;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Horario" && !colidiu)
        {
            GameObject.Find("Cilindro").SendMessage("GiraHorario");
            colidiu = true;
        }

        if (other.tag == "AntiHorario" && !colidiu)
        {
            
            GameObject.Find("Cilindro").SendMessage("GiraAntiHorario");
            colidiu = true;
        }
    }
}
