﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var x = Input.GetAxis("Horizontal2");
        var z = Input.GetAxis("Vertical2");

        transform.Translate(x * 15.0f * Time.deltaTime, 0, z * 15.0f * Time.deltaTime);
    }
}
