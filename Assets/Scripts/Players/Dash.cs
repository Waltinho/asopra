﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : MonoBehaviour {

    public DashState dashState;
    public float dashCoolDown;                        //Dash cooldown if set to 0 cooldown is maxDash * 2(counts up to max dash then back down to 0)...... if you add a number into dashCoolDown.....maxDash * 2 + dashCoolDown.
    public float maxDash = 20f;                        //How long player can dsah for.
    public float force;
    public Vector3 savedVelocity;                    //Velocity of player before the dash...used to revert back to original velocity after dash is finished.

    private Rigidbody theRB;                        //Reference to the players RigidBody.

    void Awake()
    {
        theRB = GetComponent<Rigidbody>();
    }

    void Update()
    {
        switch (dashState)
        {
            //What happens before the dash.
            case DashState.Ready:
                if (Input.GetButtonDown("Fire3"))
                {
                    //Initial velocity before the dash button is pressed.
                    savedVelocity = theRB.velocity;
                    //How FAST the player will dash.
                    theRB.velocity = new Vector3(theRB.velocity.x * force, theRB.velocity.y, theRB.velocity.z);
                    dashState = DashState.Dashing;
                }
                break;
            //What happens during the dash.
            case DashState.Dashing:
                dashCoolDown += Time.deltaTime * 3;
                if (dashCoolDown >= maxDash)
                {
                    dashCoolDown = maxDash;
                    //Reverts back to initial velocity(speed before dash).
                    theRB.velocity = new Vector3(savedVelocity.x, theRB.velocity.y, theRB.velocity.z);
                    dashState = DashState.Cooldown;
                }
                break;
            //What happens after the dash.
            case DashState.Cooldown:
                //Makes counter go down
                dashCoolDown -= Time.deltaTime;
                //Stops when timer hits 0
                if (dashCoolDown <= 0)
                {
                    //Won't let timer go below 0.
                    dashCoolDown = 0;
                    dashState = DashState.Ready;
                }
                break;
        }
    }
}

public enum DashState
{
    Ready,
    Dashing,
    Cooldown
}

